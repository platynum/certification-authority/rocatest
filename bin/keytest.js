#!/usr/bin/env node

const fs = require('node:fs');
const process = require('node:process');

const jsrsasign = require('jsrsasign');
const yargs = require('yargs');

const { rocatest, cabtest } = require('../scripts');

const argv = yargs
    .scriptName('keytest')
    .version()
    .usage('$0 [options] <file0> [<file1> ...]')
    .demandCommand(1)
    .count('verbose')
    .alias('v', 'verbose')
    .describe('verbose', 'log results to console')
    .option('roca', {
        alias: 'r',
        default: true,
        type: 'boolean',
        description: 'test key for ROCA vulnerability',
        group: 'RSA'
    })
    .option('cab', {
        alias: 'c',
        default: false,
        type: 'boolean',
        description: 'check key for CAB Baseline Requirements',
        group: 'CAB',
        implies: 'roca'
    })
    .help()
    .argv;

let exitValue = 0;
for (let filename of argv._) {
    try {
        if (argv.verbose > 1)
            console.log(`reading ${filename} ...`);
        let pem = fs.readFileSync(filename, 'utf8');
        if (argv.verbose > 1)
            console.log(`parsing ${filename} ...`);
        let key = jsrsasign.KEYUTIL.getKey(pem);
        let result = false;
        if (argv.roca) {
            result = rocatest.check(key);
            if (result === true) {
                console.log(`${filename} is ROCA vulnerable`);
                exitValue = 1;
            }
            else if (argv.verbose > 0) {
                console.log(`${filename} is not ROCA vulnerable`);
            }
        }
        if (argv.cab) {
            if (cabtest.check(key) === true || result === true) {
                console.log(`${filename} is weak according to CAB`);
                exitValue = 1;
            }
            else if (argv.verbose > 0) {
                console.log(`${filename} is ok according to CAB`);
            }
        }
    }
    catch (error) {
        exitValue = 1;
        console.error(`Cannot read ${filename} (${error})`);
    }
}
process.exit(exitValue);
