const sqrt = require("bigint-isqrt");

var cabtest = function () {
    function cabtest() {}

    /* Derived from https://github.com/letsencrypt/boulder/blob/main/goodkey/good_key.go MPL-2.0 */
    function checkPrimeFactorsTooClose(n, rounds) {
        let a = sqrt(n) + 1n;
        let b2 = (a * a) - n;
        for (let i=0; i<rounds; i++) {
            let b = sqrt(b2);
            if (b2 === (b * b)) {
                /* let p = b + a;
                let q = a - b; */
                return true;
            }
            a += 1n;
            b2 = (a * a) - n;
        }
        return false;
    }
    const primes = [
          2n,   3n,   5n,   7n,  11n,  13n,  17n,  19n,  23n,  29n,  31n,  37n,  41n,  43n,  47n,  53n,
         59n,  61n,  67n,  71n,  73n,  79n,  83n,  89n,  97n, 101n, 103n, 107n, 109n, 113n, 127n, 131n,
        137n, 139n, 149n, 151n, 157n, 163n, 167n, 173n, 179n, 181n, 191n, 193n, 197n, 199n, 211n, 223n,
        227n, 229n, 233n, 239n, 241n, 251n, 257n, 263n, 269n, 271n, 277n, 281n, 283n, 293n, 307n, 311n,
        313n, 317n, 331n, 337n, 347n, 349n, 353n, 359n, 367n, 373n, 379n, 383n, 389n, 397n, 401n, 409n,
        419n, 421n, 431n, 433n, 439n, 443n, 449n, 457n, 461n, 463n, 467n, 479n, 487n, 491n, 499n, 503n,
        509n, 521n, 523n, 541n, 547n, 557n, 563n, 569n, 571n, 577n, 587n, 593n, 599n, 601n, 607n, 613n,
        617n, 619n, 631n, 641n, 643n, 647n, 653n, 659n, 661n, 673n, 677n, 683n, 691n, 701n, 709n, 719n,
        727n, 733n, 739n, 743n, 751n
    ];
    function checkSmallPrimeFactors(n) {
        for (let prime of primes) {
            if (n % prime === 0n)
                return true;
        }
    }
    function checkModulus(sKey) {
        let n = BigInt(`0x${sKey.n.toString(16)}`);
        if (checkPrimeFactorsTooClose(n, 101))
            return true;
        if (checkSmallPrimeFactors(n))
            return true;
        return false;
    }

    function checkExponentIsOdd(e) {
        return (e % 2n === 0n);
    }
    /* "RSA key's exponent should be 3 or between 2^16+1 and 2^256-1" */
    function checkExponentRange(e) {
        return (e !== 3n && (e < (2n ** 16n + 1n) || e > (2n ** 256n - 1n)));
    }
    function checkExponent(sKey) {
        let e = BigInt(`0x${sKey.e.toString(16)}`);
        return checkExponentIsOdd(e) && checkExponentRange(e);
    }
    cabtest.check = function(sKey) {
        if (sKey.type === 'EC') {
            if (!sKey.curveName)
                sKey.curveName = sKey.ecparams.name;
            return !sKey.ecparams.G.validate();
        }
        else {
            if (sKey.n && checkModulus(sKey))
                return true;
            if (sKey.e && checkExponent(sKey))
                return true;
            return false;
        }
    }
    return cabtest;
}();

if((typeof module !== "undefined") && (module.hasOwnProperty("exports"))) {
    module.exports = cabtest;
}

if((typeof define === "function") && define.amd) {
    define("cabtest", [], function() {
        return cabtest;
    });
}

