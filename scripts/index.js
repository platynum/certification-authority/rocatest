const rocatest = require('./rocatest');
const cabtest = require('./cabtest');

if((typeof module !== "undefined") && (module.hasOwnProperty("exports"))) {
    module.exports = { rocatest, cabtest };
}

if((typeof define === "function") && define.amd) {
    define("rocatest", [], function() {
        return { rocatest, cabtest };
    });
}

