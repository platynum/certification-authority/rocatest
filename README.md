# Javascript ROCA test

This module enables you to check RSA public keys for the ROCA
vulnerability. For more information on the vulnerability, please
check https://crocs.fi.muni.cz/public/papers/rsa_ccs17.

**NOTE**: This module is based on https://github.com/fotisl/rocatest

## Download module

This module is [availbale via npm](https://www.npmjs.com/package/@platynum/rocatest)

```shell
$ npm install @platynum/rocatest
```

## Technical details

The test is based on the original moduli test. It can detect
vulnerable keys loaded using the jsrsasign https://github.com/kjur/jsrsasign
library.

## Sample usage

```javascript
const jsrsasign = require('jsrsasign');
const { rocatest } = require('@platynum/rocatest');

let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 512);
if (rocatest.check(key.pubKeyObj) === true)
    console.log('Key is vulnerable');
```

