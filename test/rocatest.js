const jsrsasign = require('jsrsasign');
const fs = require('node:fs');
const expect = require('chai').expect;

const roca = require('../scripts/rocatest');

describe('ROCA', function() {
  describe('check', function() {
    it('should recognize ROCA-1 key', function() {
      let pem = fs.readFileSync('test/roca/1.key.pem', 'utf8');
      let key = jsrsasign.KEYUTIL.getKey(pem);
      expect(roca.check(key)).to.be.true;
    });
    it('should recognize ROCA-2 key', function() {
      let pem = fs.readFileSync('test/roca/2.key.pem', 'utf8');
      let key = jsrsasign.KEYUTIL.getKey(pem);
      expect(roca.check(key)).to.be.true;
    });
    it('should pass EC key', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
      expect(roca.check(key.prvKeyObj)).to.be.false;
    });
    it('should pass generated public key (512 bit)', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 512);
      expect(roca.check(key.prvKeyObj)).to.be.false;
    });
    it('should pass generated private key (1024 bit)', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 1024);
      expect(roca.check(key.pubKeyObj)).to.be.false;
    });
    it('should pass generated private key (2048 bit)', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 2048);
      expect(roca.check(key.pubKeyObj)).to.be.false;
    });
    it('should pass generated public key (3072 bit)', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 3072);
      expect(roca.check(key.prvKeyObj)).to.be.false;
    });
    it('should pass generated public key (4096 bit)', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 4096);
      expect(roca.check(key.prvKeyObj)).to.be.false;
    });
  });
});
