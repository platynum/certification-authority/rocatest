const jsrsasign = require('jsrsasign');
const fs = require('node:fs');
const expect = require('chai').expect;

const cab = require('../scripts/cabtest');

describe('CAB', function() {
  describe('check', function() {
    it('should pass EC key', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp384r1');
      expect(cab.check(key.prvKeyObj)).to.be.false;
    });
    it('should pass generated private key (2048 bit)', function() {
      let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 2048);
      expect(cab.check(key.pubKeyObj)).to.be.false;
    });
    it('should reject key with close factors', function() {
      let pem = fs.readFileSync('test/cab/factor.pem', 'utf8');
      let key = jsrsasign.KEYUTIL.getKey(pem);
      expect(cab.check(key)).to.be.true;
    });
    const primes = [
        {value:   2},{value:   3},{value:   5},{value:   7},{value:  11},{value:  13},{value:  17},
        {value:  19},{value:  23},{value:  29},{value:  31},{value:  37},{value:  41},{value:  43},
        {value:  47},{value:  53},{value:  59},{value:  61},{value:  67},{value:  71},{value:  73},
        {value:  79},{value:  83},{value:  89},{value:  97},{value: 101},{value: 103},{value: 107},
        {value: 109},{value: 113},{value: 127},{value: 131},{value: 137},{value: 139},{value: 149},
        {value: 151},{value: 157},{value: 163},{value: 167},{value: 173},{value: 179},{value: 181},
        {value: 191},{value: 193},{value: 197},{value: 199},{value: 211},{value: 223},{value: 227},
        {value: 229},{value: 233},{value: 239},{value: 241},{value: 251},{value: 257},{value: 263},
        {value: 269},{value: 271},{value: 277},{value: 281},{value: 283},{value: 293},{value: 307},
        {value: 311},{value: 313},{value: 317},{value: 331},{value: 337},{value: 347},{value: 349},
        {value: 353},{value: 359},{value: 367},{value: 373},{value: 379},{value: 383},{value: 389},
        {value: 397},{value: 401},{value: 409},{value: 419},{value: 421},{value: 431},{value: 433},
        {value: 439},{value: 443},{value: 449},{value: 457},{value: 461},{value: 463},{value: 467},
        {value: 479},{value: 487},{value: 491},{value: 499},{value: 503},{value: 509},{value: 521},
        {value: 523},{value: 541},{value: 547},{value: 557},{value: 563},{value: 569},{value: 571},
        {value: 577},{value: 587},{value: 593},{value: 599},{value: 601},{value: 607},{value: 613},
        {value: 617},{value: 619},{value: 631},{value: 641},{value: 643},{value: 647},{value: 653},
        {value: 659},{value: 661},{value: 673},{value: 677},{value: 683},{value: 691},{value: 701},
        {value: 709},{value: 719},{value: 727},{value: 733},{value: 739},{value: 743},{value: 751}
    ];
    primes.forEach((prime) => {
      it(`rejects RSA key with modulus divisible by ${prime.value}`, function() {
        let pem = fs.readFileSync(`test/cab/${prime.value}.pem`, 'utf8');
        let key = jsrsasign.KEYUTIL.getKey(pem);
        expect(cab.check(key)).to.be.true;
      });
    });
  });
});
